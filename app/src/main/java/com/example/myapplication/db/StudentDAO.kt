package com.example.myapplication.db

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface StudentDAO {

    @Insert
    suspend fun insertStudent(student: Student): Long

    @Update
    suspend fun updateStudent(student: Student): Int

    @Delete
    suspend fun deleteStudent(student: Student): Int

    @Query(value = "DELETE FROM student_data_table")
    suspend fun deleteAll(): Int

    @Query("SELECT * FROM student_data_table")
    fun getAllStudent(): LiveData<List<Student>>


}