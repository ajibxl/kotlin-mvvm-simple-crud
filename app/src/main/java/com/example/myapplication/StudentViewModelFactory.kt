package com.example.myapplication

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.myapplication.db.StudentRepository
import java.lang.IllegalArgumentException

class StudentViewModelFactory(private val repository: StudentRepository): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if(modelClass.isAssignableFrom(StudentViewModel::class.java)){
            return StudentViewModel(repository) as T
        }

        throw IllegalArgumentException("Unknown view model class")
    }
}