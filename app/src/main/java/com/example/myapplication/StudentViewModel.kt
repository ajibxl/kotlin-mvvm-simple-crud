package com.example.myapplication


import android.util.Log
import android.util.Patterns
import androidx.databinding.Bindable
import androidx.databinding.Observable
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope

import com.example.myapplication.db.Student
import com.example.myapplication.db.StudentRepository
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class StudentViewModel(private  val repository: StudentRepository): ViewModel(), Observable {

    val student = repository.students;

    @Bindable
    val inputName = MutableLiveData<String>()
    @Bindable
    val inputEmail = MutableLiveData<String>()

    @Bindable
    val saveOrUpdateButtonText = MutableLiveData<String>()
    @Bindable
    val clearAllOrDeleteButtonText = MutableLiveData<String>()

    private var isUpdateOrDelete = false
    private lateinit var studentToUpdateOrDelete: Student
    private val statusMessage = MutableLiveData<String>()

    init {
        saveOrUpdateButtonText.value = "Save"
        clearAllOrDeleteButtonText.value = "Clear All"
    }

    fun saveOrUpdate(){
        if(isUpdateOrDelete){
            studentToUpdateOrDelete.name = inputName.value!!
            studentToUpdateOrDelete.email = inputEmail.value!!
            update(studentToUpdateOrDelete)
        }else {
            val name: String = inputName.value!!
            val email: String = inputEmail.value!!
            insert(Student(id = 0, name = name, email = email))
            inputName.value = null
            inputEmail.value = null
        }

    }

    fun clearAllOrDelete(){
        if (isUpdateOrDelete) {
            delete(studentToUpdateOrDelete)
        } else {
            clearAll()
        }
    }

    fun insert(student: Student): Job =
        viewModelScope.launch {
            repository.insert(student)
        }


    fun update(student: Student): Job =
        viewModelScope.launch {
            repository.update(student)
        }

    fun delete(student: Student): Job =
        viewModelScope.launch {
            val noOfRowsDeleted = repository.delete(student)
            if (noOfRowsDeleted > 0) {
                inputName.value = null
                inputEmail.value = null
                isUpdateOrDelete = false
                saveOrUpdateButtonText.value = "Save"
                clearAllOrDeleteButtonText.value = "Clear All"
                statusMessage.value = "$noOfRowsDeleted Row Deleted Successfully"
            } else {
                statusMessage.value = "Error Occurred"
            }

        }

    fun clearAll(): Job =
        viewModelScope.launch {
            val noOfRowsDeleted = repository.deleteAll()
            if(noOfRowsDeleted > 0){
                statusMessage.value = "$noOfRowsDeleted Subscribers Deleted Successfully"
            }else {
                statusMessage.value = "Error Occurred"
            }
        }

    fun initUpdateAndDelete(student: Student){
        inputName.value = student.name
        inputEmail.value = student.email
        isUpdateOrDelete = true;
        studentToUpdateOrDelete = student
        saveOrUpdateButtonText.value = "Update"
        clearAllOrDeleteButtonText.value = "Delete"
    }

    override fun removeOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {

    }

    override fun addOnPropertyChangedCallback(callback: Observable.OnPropertyChangedCallback?) {

    }


}