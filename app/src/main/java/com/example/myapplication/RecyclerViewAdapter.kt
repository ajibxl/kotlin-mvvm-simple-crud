package com.example.myapplication

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.databinding.ListItemBinding
import com.example.myapplication.db.Student

class RecyclerViewAdapter(private val clickListener: (Student)->Unit): RecyclerView.Adapter<MyViewHolder>() {
    private val studentsList = ArrayList<Student>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ListItemBinding =
            DataBindingUtil.inflate(layoutInflater, R.layout.list_item, parent, false)
        return MyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        Log.d("RecyclerViewAdapter", "studentsList.size "+studentsList.size);
        return studentsList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(studentsList[position], clickListener)
    }

    fun setList(students: List<Student>){
        studentsList.clear();
        studentsList.addAll(students)
    }
}

class MyViewHolder(val binding: ListItemBinding): RecyclerView.ViewHolder(binding.root){
    fun bind(student: Student, clickListener: (Student) -> Unit){
        binding.nameTextView.text = student.name
        binding.emailTextView.text = student.email
        Log.d("RecyclerViewAdapter", "xx "+binding.emailTextView.text);
        binding.listItemLayout.setOnClickListener {
            clickListener(student)
        }
    }
}