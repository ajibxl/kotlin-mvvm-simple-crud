package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.myapplication.databinding.ActivityMainBinding
import com.example.myapplication.db.Student
import com.example.myapplication.db.StudentDAO
import com.example.myapplication.db.StudentDatabase
import com.example.myapplication.db.StudentRepository

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var studentViewModel: StudentViewModel
    private lateinit var adapter: RecyclerViewAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        val dao: StudentDAO = StudentDatabase.getInstance(application).studentDAO
        val repository = StudentRepository(dao)
        val factory = StudentViewModelFactory(repository)
        studentViewModel = ViewModelProvider(this, factory).get(StudentViewModel::class.java)
        binding.myViewModel = studentViewModel
        binding.lifecycleOwner = this
        initRecyclerView()
    }

    private fun displayStudentList(){
        studentViewModel.student.observe(this, Observer {
            Log.i("MainAct",it.toString() )
//            binding.studentRecyclerview.adapter = RecyclerViewAdapter(it)
            adapter.setList(it)
            adapter.notifyDataSetChanged()
        })
    }

    private fun initRecyclerView(){
        binding.studentRecyclerview.layoutManager = LinearLayoutManager(this)
        adapter = RecyclerViewAdapter({selectedItem: Student->listItemCLicked(selectedItem)})
        binding.studentRecyclerview.adapter = adapter;
        displayStudentList()
    }

    private fun listItemCLicked(student: Student){
        studentViewModel.initUpdateAndDelete(student)
    }
}